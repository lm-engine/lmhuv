#pragma once

#include <entt/entity/registry.hpp>

#include <lmlib/geometry.h>
#include <lmlib/reference.h>
#include <lmtk/lmtk.h>
#include <lmtk/resource_sink.h>

namespace lmhuv
{
class ivisual_view
{
  public:
    virtual void add_to_frame(
      entt::registry const &registry,
      lmgl::iframe *frame,
      lmgl::viewport const &viewport) = 0;

    virtual void move_resources(
      lmtk::resource_sink &resource_sink,
      lmgl::irenderer *renderer) = 0;

    virtual ivisual_view &
      clear(lmgl::irenderer *renderer, lmtk::resource_sink &resource_sink) = 0;

    virtual void add_box(lmgl::irenderer *renderer, entt::entity entity) = 0;

    virtual void destroy_box(
      lmgl::irenderer *renderer,
      entt::entity entity,
      lmtk::resource_sink &resource_sink) = 0;
};

using pvisual_view = lm::reference<ivisual_view>;

struct visual_view_init
{
    entt::registry const &registry;
    lmgl::irenderer *renderer;
};

pvisual_view create_visual_view(visual_view_init const &init);
} // namespace lmhuv
