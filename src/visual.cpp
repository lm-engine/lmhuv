#include "visual.h"
#include "rendering/shapes.h"

#include <lmhuv.h>
#include <lmhuv/box.h>

namespace lmhuv::internal
{
visual::visual(visual_view_init const &init)
    : box_material{lmhuv::create_box_material(init.renderer)},
      light_direction{Eigen::Vector3f{0.2f, 0.6f, -0.75f}.normalized()}
{
    std::tie(box_vpositions, box_vnormals, box_indices, n_box_indices) =
      lmhuv::create_box_buffers(init.renderer);

    add_box_meshes(init.registry, init.renderer);
}

void visual::add_box_meshes(
  entt::registry const &registry,
  lmgl::irenderer *renderer)
{
    registry.view<lmng::box_render const>().each(
      [&](entt::entity entity, lmng::box_render const &box_component) {
          add_box_mesh(renderer, entity);
      });
}

void visual::add_box_mesh(lmgl::irenderer *renderer, entt::entity &entity)
{
    auto ubuffer = create_box_ubuffer(renderer);
    auto geometry = renderer->create_geometry(lmgl::geometry_init{
      .material = box_material,
      .vertex_buffers =
        {
          box_vpositions.get(),
          box_vnormals.get(),
        },
      .index_buffer = box_indices.get(),
      .uniform_buffer = ubuffer.get(),
    });
    geometry->set_n_indices(n_box_indices);
    box_meshes.emplace(
      entity,
      box_mesh{
        std::move(ubuffer),
        std::move(geometry),
      });
}

void visual::add_to_frame(
  entt::registry const &registry,
  lmgl::iframe *frame,
  lmgl::viewport const &viewport)
{
    registry.view<lmng::box_render const, lmng::transform const>().each(
      [&](
        auto entity,
        lmng::box_render const &box,
        lmng::transform const &transform) {
          auto &box_mesh = box_meshes.at(entity);
          update_box_uniform(
            frame,
            box_mesh.ubuffer.get(),
            registry.ctx<lm::camera>(),
            transform,
            box,
            box.colour,
            light_direction);
          frame->add({box_mesh.geometry.get()}, viewport);
      });
}

void visual::move_resources(
  lmtk::resource_sink &resource_sink,
  lmgl::irenderer *renderer)
{
    resource_sink.add(
      renderer, box_vpositions, box_vnormals, box_indices, box_material);
    clear(renderer, resource_sink);
}

void visual::add_box(lmgl::irenderer *renderer, entt::entity entity)
{
    add_box_mesh(renderer, entity);
}

void visual::destroy_box(
  lmgl::irenderer *renderer,
  entt::entity entity,
  lmtk::resource_sink &resource_sink)
{
    auto &mesh = box_meshes.at(entity);
    resource_sink.add(renderer, mesh.ubuffer);
    resource_sink.add(renderer, mesh.geometry);
    box_meshes.erase(entity);
}

visual &
  visual::clear(lmgl::irenderer *renderer, lmtk::resource_sink &resource_sink)
{
    for (auto &key_val : box_meshes)
        resource_sink.add(
          renderer, key_val.second.ubuffer, key_val.second.geometry);
    return *this;
}
} // namespace lmhuv::internal

namespace lmhuv
{
pvisual_view create_visual_view(visual_view_init const &init)
{
    return std::make_unique<internal::visual>(init);
}
} // namespace lmhuv
