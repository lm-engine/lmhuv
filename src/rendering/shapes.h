#pragma once

#include <lmengine/shapes.h>
#include <lmengine/transform.h>
#include <lmgl/fwd_decl.h>
#include <lmgl/lmgl.h>
#include <lmlib/camera.h>

namespace lmhuv
{
lmgl::material
  create_box_material(lmgl::irenderer *renderer, bool write_stencil = false);

lmgl::buffer create_box_ubuffer(lmgl::irenderer *renderer);

void update_box_uniform(
  lmgl::iframe *frame,
  lmgl::ibuffer *buffer,
  lm::camera const &camera,
  lmng::transform const &transform,
  lmng::box_render const &box,
  std::array<float, 3> colour,
  Eigen::Vector3f const &light_direction);
} // namespace lmhuv
