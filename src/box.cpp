#include <lmengine/shapes.h>
#include <lmengine/transform.h>
#include <lmhuv/box.h>
#include <lmlib/camera.h>

namespace lmhuv
{
Eigen::Matrix4f get_box_world_matrix(
  lmng::transform const &transform,
  lmng::box_render const &box)
{
    return (Eigen::Translation3f{transform.position} *
            Eigen::Affine3f(transform.rotation) * Eigen::Scaling(box.extents))
      .matrix();
}

Eigen::Matrix4f get_box_mvp_matrix(
  lmng::transform const &transform,
  lmng::box_render const &box,
  lm::camera const &camera)
{
    return camera.view_matrix() *
           (Eigen::Translation3f{transform.position} *
            Eigen::Affine3f(transform.rotation) * Eigen::Scaling(box.extents))
             .matrix();
}
} // namespace lmhuv
